찬송가 265장 (주 십자가를 지심으로/耶稣我救主作神羔)
https://www.hymnal.net/en/hymn/ch/685?gb=1
1 耶稣我救主作神羔羊
  为罪人受死，罪债清偿
  将祂的宝血洒你身上
  神必越过你，你免灭亡

  我一见这血
  我一见这血
  我一见这血
  我就必越过你，不毁灭

2 罪中的罪魁，耶稣肯救
  凡祂所应许，祂必成就
  在祂这血下，虽有愆尤
  神必越过你，你得宽宥
  
  我一见这血
  我一见这血
  我一见这血
  我就必越过你，不毁灭

3 审判要来到，无人免除
  在神公义前，谁能立足
  惟有让祂血将你掩护
  神必越过你，你免对簿

  我一见这血
  我一见这血
  我一见这血
  我就必越过你，不毁灭

4 哦，怜悯何大！哦，爱无边
  哦，神圣恩典，丰盛、甘甜
  在血荫蔽下享受平安
  神必越过你，你要颂赞

  奇妙羔羊血
  荣耀羔羊血
  宝贵羔羊血
  神一见羔羊血便逾越
